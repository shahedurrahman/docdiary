﻿using DocDiary.Business;
using DocDiary.Entities;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DocDiary.Service.Controllers
{
    public class DoctorController : ApiController
    {
        public HttpResponseMessage Create(Doctor doctor)
        {
            try
            {
                int ret = BusinessFactory.DoctorBusiness.Create(doctor);

                if (ret > 0)
                {
                    var response = Request.CreateResponse(HttpStatusCode.Created);
                    string uri = Url.Route(null, new { action = "Get", id = ret });
                    response.Headers.Location = new Uri(Request.RequestUri, uri);

                    return response;
                }

                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, string.Empty));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message));
            }
        }

        public HttpResponseMessage Update()
        {
            throw new NotImplementedException();
        }

        public HttpResponseMessage Delete(int id)
        {
            return new HttpResponseMessage(HttpStatusCode.NoContent);
        }

        public HttpResponseMessage Get(int id)
        {
            try
            {
                var doctor = BusinessFactory.DoctorBusiness.Get<Doctor>(id);

                if (doctor == null)
                {
                    //TODO: Make static message dynamic
                    throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.NotFound, HttpErrorMessages.NotFound));
                }

                return Request.CreateResponse(HttpStatusCode.OK, doctor);
            }
            catch (HttpResponseException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message));
            }
        }

        public HttpResponseMessage GetAll()
        {
            try
            {
                List<Doctor> doctors = BusinessFactory.DoctorBusiness.GetAll<Doctor>();

                if (doctors == null)
                {
                    throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.NotFound, HttpErrorMessages.NotFound));
                }

                return Request.CreateResponse(HttpStatusCode.OK, doctors);
            }
            catch (HttpResponseException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message));
            }
        }
    }
}
