﻿using DocDiary.Business;
using DocDiary.Entities;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DocDiary.Service.Controllers
{
    public class SpecialityController : ApiController
    {
        public HttpResponseMessage Create(Speciality speciality)
        {
            try
            {
                int ret = BusinessFactory.SpecialityBusiness.Create(speciality);

                if (ret > 0)
                {
                    var response = Request.CreateResponse(HttpStatusCode.Created);
                    string uri = Url.Route(null, new { action = "Get", id = ret });
                    response.Headers.Location = new Uri(Request.RequestUri, uri);

                    return response;
                }

                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, string.Empty));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message));
            }
        }

        public HttpResponseMessage Update()
        {
            throw new NotImplementedException();
        }

        public HttpResponseMessage Delete(int id)
        {
            return new HttpResponseMessage(HttpStatusCode.NoContent);
        }

        public HttpResponseMessage Get(int id)
        {
            try
            {
                var speciality = BusinessFactory.SpecialityBusiness.Get<Speciality>(id);

                if (speciality == null)
                {
                    // TODO: Make static message dynamic
                    throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.NotFound, HttpErrorMessages.NotFound));
                }

                return Request.CreateResponse(HttpStatusCode.OK, speciality);
            }
            catch (HttpResponseException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message));
            }
        }

        public HttpResponseMessage GetAll()
        {
            try
            {
                var specialities = BusinessFactory.SpecialityBusiness.GetAll<Speciality>();

                if (specialities == null)
                {
                    throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.NotFound, HttpErrorMessages.NotFound));
                }

                return Request.CreateResponse(HttpStatusCode.OK, specialities);
            }
            catch (HttpResponseException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message));
            }
        }
    }
}
