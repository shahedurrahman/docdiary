﻿using DocDiary.Shared;
using System;

namespace DocDiary.Entities
{
    public class BaseEntity
    {
        [DataValidation(Exclude = true)]
        public DateTime UpdateDate { get; set; }

        [DataValidation(Required = true)]
        public int UpdatedBy { get; set; }
    }
}
