﻿using DocDiary.Shared;

namespace DocDiary.Entities
{
    public class Doctor : User
    {
        [DataValidation(Required = true)]
        public string Title { get; set; }

        [DataValidation(Required = false)]
        public string Description { get; set; }

        [DataValidation(Required = false)]
        public byte[] Photo { get; set; }

        [DataValidation(Required = true)]
        public int TotalExperience { get; set; }

        [DataValidation(Required = true)]
        public int SpecialityId { get; set; }
    }
}
