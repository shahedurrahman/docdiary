﻿using DocDiary.Shared;
using System;

namespace DocDiary.Entities
{
    public class User
    {
        [DataValidation(Required = true)]
        public int Id { get; set; }

        [DataValidation(Required = true)]
        public string Email { get; set; }

        [DataValidation(Required = true)]
        public string FirstName { get; set; }

        [DataValidation(Required = true)]
        public string LastName { get; set; }

        [DataValidation(Required = true)]
        public string Phone { get; set; }

        [DataValidation(Required = false)]
        public string Fax { get; set; }

        [DataValidation(Required = false)]
        public string Address { get; set; }

        [DataValidation(Required = false)]
        public DateTime BirthDate { get; set; }
    }
}
