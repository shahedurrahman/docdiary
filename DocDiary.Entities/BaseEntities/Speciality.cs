﻿using DocDiary.Shared;

namespace DocDiary.Entities
{
    public class Speciality
    {
        [DataValidation(Required = true)]
        public int? Id { get; set; }

        [DataValidation(Required = true, MaxLength = 100)]
        public string Name { get; set; }
    }
}
