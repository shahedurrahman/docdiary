﻿using DocDiary.Shared;

namespace DocDiary.Entities.BaseEntities
{
    public class ErrorLog
    {
        [DataValidation(Exclude = true)]
        public int Id { get; set; }

        [DataValidation(Required = true)]
        public string Message { get; set; }

        [DataValidation(Required = true)]
        public string ExceptionType { get; set; }

        [DataValidation(Required = true)]
        public string StackTrace { get; set; }
    }
}
