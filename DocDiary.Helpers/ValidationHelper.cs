﻿using DocDiary.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;

namespace DocDiary.Helpers
{
    public class ValidationHelper
    {
        public static bool ValidateEntity<T>(T t)
        {
            var properties = new List<PropertyInfo>(GetObjectProperties(typeof(T)));

            foreach (PropertyInfo p in properties)
            {
                string columnName = p.Name;
                object value = p.GetValue(t, null);

                object[] attributes = p.GetCustomAttributes(true);

                foreach (object obj in attributes)
                {
                    // TODO: check if current attribute is DataValidationAttribute
                    var dbAttribute = obj as DataValidationAttribute;

                    if (dbAttribute != null)
                    {
                        if (!dbAttribute.Exclude)
                        {
                            if (dbAttribute.Required && (string.IsNullOrWhiteSpace(Convert.ToString(value))))
                                throw new ArgumentException("Value for property '" + columnName + "' is not supplied.");

                            if (dbAttribute.MinLength > 0)
                            {
                                if (value.ToString().Length < dbAttribute.MinLength)
                                    throw new ArgumentException("Value for property '" + columnName + "' should be at least '" + dbAttribute.MinLength + "' character long.");
                            }

                            if (dbAttribute.MaxLength > 0)
                            {
                                if (value.ToString().Length > dbAttribute.MaxLength)
                                    throw new ArgumentException("Value for property '" + columnName + "' should not be more than '" + dbAttribute.MaxLength + "' character long.");
                            }
                        }
                    }
                }
            }

            return true;
        }

        private static IEnumerable<PropertyInfo> GetObjectProperties(Type type)
        {
            var properties = new List<PropertyInfo>();
            PropertyDescriptorCollection pdc = TypeDescriptor.GetProperties(type);

            foreach (PropertyDescriptor item in pdc)
                if (item.IsBrowsable)
                    properties.Add(type.GetProperty(item.Name));

            return properties;
        }
    }
}
