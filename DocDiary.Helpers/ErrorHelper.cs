﻿using DocDiary.Data;
using DocDiary.Entities.BaseEntities;
using System;

namespace DocDiary.Helpers
{
    public class ErrorHelper
    {
        public static int Log(Exception exception)
        {
            var errorLog = new ErrorLog
            {
                Message = exception.Message,
                ExceptionType = exception.GetType().Name,
                StackTrace = exception.StackTrace
            };

            DbHelper dbHelper = DbHelper.Begin(true);
            int ret = dbHelper.ExecuteNonQuery(StoredProcedures.ErrorLogProcedure.Create, errorLog);
            dbHelper.End();

            return ret;
        }
    }
}
