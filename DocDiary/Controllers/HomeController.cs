﻿using System.Web.Mvc;

namespace DocDiary.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/
        public ActionResult Index()
        {
            ViewBag.Title = "Doctor's Diary";

            return View();
        }
    }
}