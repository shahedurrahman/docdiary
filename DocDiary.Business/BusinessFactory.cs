﻿using DocDiary.Business.Implementation;

namespace DocDiary.Business
{
    public static class BusinessFactory
    {
        public static readonly SpecialityBusiness SpecialityBusiness = new SpecialityBusiness();
        public static readonly DoctorBusiness DoctorBusiness = new DoctorBusiness();
    }
}
