﻿using DocDiary.Business.Interface;
using DocDiary.Data;
using DocDiary.Helpers;
using System;
using System.Collections.Generic;

namespace DocDiary.Business.Implementation
{
    public class DoctorBusiness : IBaseBusiness
    {
        public int Create<T>(T entity)
        {
            int ret = 0;

            try
            {
                if (ValidationHelper.ValidateEntity(entity))
                {
                    DbHelper dbHelper = DbHelper.Begin(true);
                    ret = DataFactory.DoctorData.Create(entity, dbHelper);
                    dbHelper.End();
                }

                return ret;
            }
            catch (ArgumentException)
            {
                throw;
            }
            catch (Exception ex)
            {
                ErrorHelper.Log(ex);
                throw;
            }
        }

        public int Update<T>(T entity)
        {
            throw new NotImplementedException();
        }

        public int Delete(int id)
        {
            throw new NotImplementedException();
        }

        public T Get<T>(int id) where T : new()
        {
            try
            {
                DbHelper dbHelper = DbHelper.Begin(false);
                T doctor = DataFactory.DoctorData.Get<T>(id, dbHelper);
                dbHelper.End();

                return doctor;
            }
            catch (Exception ex)
            {
                ErrorHelper.Log(ex);
                throw;
            }
        }

        public List<T> GetAll<T>() where T : new()
        {
            try
            {
                DbHelper dbHelper = DbHelper.Begin(false);
                List<T> doctors = DataFactory.DoctorData.GetAll<T>(dbHelper);
                dbHelper.End();

                return doctors;
            }
            catch (Exception)
            {
                // TODO: Log error in database
                throw new Exception("An internal error has occured, could not return data.");
            }
        }
    }
}
