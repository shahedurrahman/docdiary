﻿using System.Collections.Generic;

namespace DocDiary.Business.Interface
{
    interface IBaseBusiness
    {
        int Create<T>(T entity);

        int Update<T>(T entity);

        int Delete(int id);

        T Get<T>(int id) where T : new();

        List<T> GetAll<T>() where T : new();
    }
}
