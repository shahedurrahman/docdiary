﻿using DocDiary.Data.Interface;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace DocDiary.Data.Implementation
{
    public class DoctorData : IBaseData
    {
        public int Create<T>(T entity, DbHelper dbHelper)
        {
            var id = dbHelper.ExecuteScalar(StoredProcedures.DoctorProcedure.Create, entity);

            return Convert.ToInt32(id);
        }

        public int Update<T>(T entity, DbHelper dbHelper)
        {
            throw new NotImplementedException();
        }

        public int Delete(int id, DbHelper dbHelper)
        {
            throw new NotImplementedException();
        }

        public T Get<T>(int id, DbHelper dbHelper) where T : new()
        {
            var parameters = new List<SqlParameter> { new SqlParameter("Id", id) };
            DataTable dataTable = dbHelper.ExecuteDataTable(StoredProcedures.DoctorProcedure.Get, parameters);

            return dataTable.ToObject<T>();
        }

        public List<T> GetAll<T>(DbHelper dbHelper) where T : new()
        {
            DataTable dataTable = dbHelper.ExecuteDataTable(StoredProcedures.DoctorProcedure.GetAll, null);

            return dataTable.ToList<T>();
        }
    }
}
