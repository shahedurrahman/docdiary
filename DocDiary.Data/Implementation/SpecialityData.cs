﻿using DocDiary.Data.Interface;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace DocDiary.Data.Implementation
{
    public class SpecialityData : IBaseData
    {
        public int Create<T>(T entity, DbHelper dbHelper)
        {
            return Convert.ToInt32(dbHelper.ExecuteScalar(StoredProcedures.SpecialityProcedure.Create, entity));
        }

        public int Update<T>(T entity, DbHelper dbHelper)
        {
            throw new NotImplementedException();
        }

        public int Delete(int id, DbHelper dbHelper)
        {
            throw new NotImplementedException();
        }

        public T Get<T>(int id, DbHelper dbHelper) where T : new()
        {
            var parameters = new List<SqlParameter> { new SqlParameter("Id", id) };
            DataTable dataTable = dbHelper.ExecuteDataTable(StoredProcedures.SpecialityProcedure.Get, parameters);

            return dataTable.ToObject<T>();
        }

        public List<T> GetAll<T>(DbHelper dbHelper) where T : new()
        {
            DataTable dataTable = dbHelper.ExecuteDataTable(StoredProcedures.SpecialityProcedure.GetAll, null);

            return dataTable.ToList<T>();
        }
    }
}
