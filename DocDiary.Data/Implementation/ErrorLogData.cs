﻿using DocDiary.Data.Interface;
using System;
using System.Collections.Generic;

namespace DocDiary.Data.Implementation
{
    public class ErrorLogData : IBaseData
    {
        public int Create<T>(T entity, DbHelper dbHelper)
        {
            return dbHelper.ExecuteNonQuery(StoredProcedures.ErrorLogProcedure.Create, entity);
        }

        public int Update<T>(T entity, DbHelper dbHelper)
        {
            throw new NotImplementedException();
        }

        public int Delete(int id, DbHelper dbHelper)
        {
            throw new NotImplementedException();
        }

        public T Get<T>(int id, DbHelper dbHelper) where T : new()
        {
            throw new NotImplementedException();
        }

        public List<T> GetAll<T>(DbHelper dbHelper) where T : new()
        {
            throw new NotImplementedException();
        }
    }
}
