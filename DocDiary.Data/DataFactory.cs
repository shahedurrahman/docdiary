﻿using DocDiary.Data.Implementation;

namespace DocDiary.Data
{
    public static class DataFactory
    {
        public static readonly SpecialityData SpecialityData = new SpecialityData();
        public static readonly DoctorData DoctorData = new DoctorData();
    }
}
