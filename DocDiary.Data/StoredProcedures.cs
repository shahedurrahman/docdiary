﻿
namespace DocDiary.Data
{
    public static class StoredProcedures
    {
        public static class DoctorProcedure
        {
            public const string Create = "[DoctorCreate]";
            public const string Update = "[DoctorUpdate]";
            public const string Delete = "[DoctorDelete]";
            public const string Get = "[DoctorGet]";
            public const string GetAll = "[DoctorGetAll]";
        }

        public static class SpecialityProcedure
        {
            public const string Create = "[SpecialityCreate]";
            public const string Get = "[SpecialityGet]";
            public const string GetAll = "[SpecialityGetAll]";
        }

        public static class ErrorLogProcedure
        {
            public const string Create = "[ErrorLogCreate]";
        }
    }
}
