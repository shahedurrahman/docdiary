﻿using System.Collections.Generic;

namespace DocDiary.Data.Interface
{
    public interface IBaseData
    {
        int Create<T>(T entity, DbHelper dbHelper);

        int Update<T>(T entity, DbHelper dbHelper);

        int Delete(int id, DbHelper dbHelper);

        T Get<T>(int id, DbHelper dbHelper) where T : new();

        List<T> GetAll<T>(DbHelper dbHelper) where T : new();
    }
}
