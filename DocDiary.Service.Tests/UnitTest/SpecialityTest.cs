﻿using DocDiary.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net;

namespace DocDiary.Service.Tests
{
    /// <summary>
    /// Summary description for SpecialityTest
    /// </summary>
    [TestClass]
    public class SpecialityTest
    {
        private TestContext _testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return _testContextInstance;
            }
            set
            {
                _testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void SpecialityCreate_ShouldReturnCreated()
        {
            var speciality = new Speciality();
            speciality.Id = 0;
            speciality.Name = "UnitTest";
            var response = HttpClientHelper.PostJson("api/Speciality/Create", speciality);

            Assert.IsTrue(response.StatusCode == HttpStatusCode.Created);
        }

        [TestMethod]
        public void SpecialityCreate_ShouldReturnInternalServerError()
        {
            var speciality = new Speciality();
            var response = HttpClientHelper.PostJson("api/Speciality/Create", speciality);

            Assert.IsTrue(response.StatusCode == HttpStatusCode.InternalServerError);
        }

        [TestMethod]
        public void SpecialityGet_ShouldReturnOK()
        {
            var response = HttpClientHelper.GetJson("api/Speciality/Get/1");

            Assert.IsTrue(response.StatusCode == HttpStatusCode.OK);
        }

        [TestMethod]
        public void SpecialityGet_ShouldReturnNotFound()
        {
            var response = HttpClientHelper.GetJson("api/Speciality/Get/0");

            Assert.IsTrue(response.StatusCode == HttpStatusCode.NotFound);
        }
    }
}
