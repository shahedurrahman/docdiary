﻿using DocDiary.Helpers;
using System;
using System.Net.Http;

namespace DocDiary.Service.Tests
{
    class HttpClientHelper
    {
        static readonly string _baseAddress = ConfigurationHelper.GetAppSettingValue("APIBaseAddress");

        public static HttpResponseMessage GetJson(string uri)
        {
            var client = new HttpClient();
            client.BaseAddress = new Uri(_baseAddress);
            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

            return client.GetAsync(uri).Result;
        }

        public static HttpResponseMessage PostJson<T>(string uri, T entity)
        {
            var client = new HttpClient();
            client.BaseAddress = new Uri(_baseAddress);
            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

            return client.PostAsJsonAsync(uri, entity).Result;
        }
    }
}
