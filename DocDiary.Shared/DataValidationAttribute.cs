﻿using System;

namespace DocDiary.Shared
{
    public class DataValidationAttribute : Attribute
    {
        public bool Required { get; set; }

        public bool Exclude { get; set; }

        public int MaxLength { get; set; }

        public int MinLength { get; set; }
    }
}
